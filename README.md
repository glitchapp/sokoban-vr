
Sokoban for Vr

![Screenshot 1](/screenshots/sokobanvr.png)

Controls
========
Use arrows or WASD to move.

# Credits

	[![Credits](https://codeberg.org/glitchapp/sokoban-vr/src/branch/main/credits.md)]

# License

 Copyright (C) 2022  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

# Donations

If you like this remake and you want to support the development - please consider to donate. Any donations are greatly appreciated.

![Zcash address:](https://codeberg.org/glitchapp/pages/raw/branch/main/otherProjects/img/Zc.webp) t1h9UejxbLwJjCy1CnpbYpizScFBGts5J3N
![Zcash Qr Code:](https://codeberg.org/glitchapp/pages/raw/branch/main/otherProjects/img/ZCashQr160.webp)

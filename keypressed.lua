keypressed = Object:extend()

            
--find box location


function lovr.keypressed(key) 
 
 
    
    --if key=="q" then os.exit() 
  
	if key=="t" and touchinterfaceison==true then touchinterfaceison=false  elseif key=="t" and touchinterfaceison==false then touchinterfaceison=true
    
	elseif key=="h" and helpison==false then helpison=true elseif key=="h" and helpison==true then helpison=false 
	  
    elseif key=="c" and cameraon==false then cameraon=true	elseif key=="c" and  cameraon==true then cameraon=false
    
	elseif key=="p" and gamestate=="menu" then gamestate="game" menuengine.disable() elseif key=="p" and gamestate=="game" then gamestate="menu" menuengine.enable()
    
    elseif key=="n" and gamestate=="game" then gamestate="preview" 				elseif key=="n" and gamestate=="preview" then gamestate="game" displayalllevelsreturn()
	
	elseif key == 'r' then loadLevel() 

    elseif key == '1' and currentLevel>1 then currentLevel = currentLevel - 1 loadLevel()
    
    elseif key == '2' and currentLevel < 101 then currentLevel = currentLevel + 1 changelevel:loadLevel(dt)
    
    elseif key == '3' and model3d=="yes" then model3d="no" elseif key == '3' and not(model3d=="yes") then model3d="yes"
    
    elseif key == '4' and shaderselect=="moonshine" then shaderselect="shadertoy" elseif key =="4" and shaderselect=="shadertoy" then shaderselect="moonshine"
    
    elseif key == '5' and currentLevel < 95 then currentLevel = currentLevel + 5 loadLevel()
    
    --elseif key == 's' then shaderData,iSystem = loadShader("shader/balls.glsl")
    
    end
    
    
    --[[if (currentLevel<5) then love.graphics.setBackgroundColor(0, 0, 0)
	elseif (currentLevel<10) then love.graphics.setBackgroundColor(0, 0, 0)
    elseif currentLevel<15 then love.graphics.setBackgroundColor(1, 1, 1)
    elseif currentLevel<20 then love.graphics.setBackgroundColor(0.6, 0.9, 0.3)
    elseif currentLevel<25 then love.graphics.setBackgroundColor(0.9, 0.9, 0.9)
    elseif currentLevel<30	then love.graphics.setBackgroundColor(0.60, 0.67, 0.99)
    elseif currentLevel<35 then love.graphics.setBackgroundColor(1, 1, 1)
    elseif currentLevel<40 then love.graphics.setBackgroundColor(0.039,0.47, 0.6)
    elseif currentLevel<45 then love.graphics.setBackgroundColor(1, 1, 0)
    elseif currentLevel<50 then love.graphics.setBackgroundColor(0.65, 0.65, 0)
    elseif currentLevel<55 then love.graphics.setBackgroundColor(0.15, 0.18, 0.30)
    elseif currentLevel<60 then love.graphics.setBackgroundColor(0, 0, 0)
    elseif currentLevel<65 then love.graphics.setBackgroundColor(0.30, 0.18, 0.15)
    elseif currentLevel<70 then love.graphics.setBackgroundColor(0.30, 0.18, 0.15)
    elseif currentLevel<75 then love.graphics.setBackgroundColor(1, 1, 1)
    elseif currentLevel<80 then love.graphics.setBackgroundColor(0.15, 0.30, 0.18)
    elseif currentLevel<85 then love.graphics.setBackgroundColor(0.3, 0.3, 0.3)
    elseif currentLevel<90 then love.graphics.setBackgroundColor(0.15, 0.10, 0.30)
    elseif currentLevel<95 then love.graphics.setBackgroundColor(0.97, 0.02,73)
    elseif currentLevel<101 then love.graphics.setBackgroundColor(0.3, 0, 0.3)
    end--]]
  

  
  --controls
  
   if key == 'up' or key == 'down' or key == 'left' or key == 'right' then
   --findboxes() --print position of boxes for debug
   --findplayer() --print position of boxes for debug
        local playerX   -- Player position and
        local playerY   -- Adjacent position in the direction of the arrow key pressed
        --findbox()
        for testY, row in ipairs(level) do
            for testX, cell in ipairs(row) do
                if cell == player or cell == playerOnStorage then
                    playerX = testX
                    playerY = testY
                end
            end
        end
--find cell type below box

        
        --print"______________________________________"
        local dx = 0
        local dy = 0
        if key == 'left' then
            dx = -1
        elseif key == 'right' then
            dx = 1
        elseif key == 'up' then
        --elseif  joystick:isGamepadDown("dpup") then
            dy = 1
        elseif key == 'down' then
            dy = -1
		end

        local current = level[playerY][playerX]
        local adjacent = level[playerY + dy][playerX + dx]
        local beyond
        --Pushing box on to empty location
        if level[playerY + dy + dy] then
            beyond = level[playerY + dy + dy][playerX + dx + dx]
        end

        local nextAdjacent = {
            [empty] = player,
            [storage] = playerOnStorage,
        }

        local nextCurrent = {
            [player] = empty,
            [playerOnStorage] = storage,
        }

        local nextBeyond = {
            [empty] = box,
            [storage] = boxOnStorage,
        }

        local nextAdjacentPush = {
            [box] = player,
            [boxOnStorage] = playerOnStorage,
        }

        if nextAdjacent[adjacent] then
            level[playerY][playerX] = nextCurrent[current]
            level[playerY + dy][playerX + dx] = nextAdjacent[adjacent]

        elseif nextBeyond[beyond] and nextAdjacentPush[adjacent] then
            level[playerY][playerX] = nextCurrent[current]
            level[playerY + dy][playerX + dx] = nextAdjacentPush[adjacent]
            level[playerY + dy + dy][playerX + dx + dx] = nextBeyond[beyond]
        end

        local complete = true

        for y, row in ipairs(level) do
            for x, cell in ipairs(row) do
                if cell == box then
                    complete = false
                end
            end
        end

        if complete then
            currentLevel = currentLevel + 1
            if currentLevel > #levels then
                currentLevel = 1
            end
            changelevel:loadLevel(dt)
        end

        
        interfaceison=false
    end
    
    end
    
    


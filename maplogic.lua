    maplogic = Object:extend()

    player = '@'
    playerOnStorage = '+'
    box = '$'
    boxOnStorage = '*'
    storage = '.'
    wall = '#'
    empty = ' '
    boxX=0
    boxY=0
    cellSize = 32
    tileSize=0.8
    spacex=0
    spacey=0
    


    function sleep(s)
        local ntime = os.clock() + s/10
        repeat until os.clock() > ntime
      end
      
  require ('joystickpressed')
  
  require ('keypressed')           
  
  --require ('touchpressed')
  
  --require ('mousepressed')
  
  --require ('help')
  
  touchinterfaceison=false
  
cellSize = 6 tileSize=4
--Drawing a level
function maplogic:draw()
		lovr.graphics.setColor(1,1,1,1)
		--lovr.graphics.skybox(skybox)

          
lovr.graphics.transform(-1,0,-2, 0.05, 0.05, 0.05)

  --dialog1:draw()
    for y, row in ipairs(level) do
        for x, cell in ipairs(row) do
            if cell ~= empty then
                --local cellSize = 23
--set colors

local colors = {
                    [player] = {219,73,73},
                    [playerOnStorage] = {182,146,109},
                    [box] = {29,43,83},
                    [boxOnStorage] = {0.59, 1, .5},
                    [storage] = {238,202,165},
                    [wall] = {255, 255,255},
                    [empty] = {146, 73, 73},
                }               
--set tiles
				if cell==player then
                    tilepng=playerpngmat
                elseif cell==playerOnStorage then
                    tilepng=playeronstrpngmat
                elseif cell==box then
                    tilepng=boxpngmat
                elseif cell==boxOnStorage then
                    tilepng=boxonstrpngmat
                elseif cell==wall then
                tilepng=wallpngmat
                elseif cell==storage then
                    tilepng=strpngmat
                elseif cell==empty then
                    tilepng=floorpngmat
                end

        --camera
                --if 		cell==player and x<10 then  spacex=x-7
				--elseif  cell==player and x>10 then  spacex=x-10
                --end
                
                --if	cell==player and y<7 then spacey=0
                --elseif 	cell==player and y>7 then  spacey=y-7
                --end
                
                
                --if currentLevel==1 then cellSize = 6 tileSize=4            
                --elseif currentLevel==39 then cellSize = 6 tileSize=4
                --elseif currentLevel==41 then cellSize = 32 tileSize=0.8               
                --elseif currentLevel==42 then cellSize = 6 tileSize=4
                
                --elseif currentLevel==47 then cellSize = 6 tileSize=4
                --elseif currentLevel==54 then cellSize = 6 tileSize=4
                --elseif currentLevel==56 then cellSize = 6 tileSize=4
                --elseif currentLevel==59 then cellSize = 6 tileSize=4
                --end

					--colors for 3d Models
					--[player] = {0,1,0},
                    --[playerOnStorage] = {0.5,1,0.5},
                    --[box] = {1,0,0},
                    --[boxOnStorage] = {1,1,0},
                    --[storage] = {0,0,1},
                    --[wall] = {0.8,0.6,0.8},
                    --[empty] = {0, 0, 0},
				lovr.graphics.setColor(1,1,1)
				--enviromodel:draw (0,0,220,1)
				--envibuildmodel:draw (250,0,-200,35)


				if cell==player and model3d=="yes" then
				lovr.graphics.setColor(0,1,0)
				playermodel:draw((x-3-spacex) * cellSize,
				(y-spacey) * cellSize,
				2,0.25)
				elseif (cell==player and not(model3d=="yes")) then
                lovr.graphics.setColor(colors[cell])
            	lovr.graphics.box(
				tilepng,
				(x-3-spacex) * cellSize,
				(y-spacey) * cellSize,
				2,
				tileSize,tilesize,0.1)
				
				elseif cell==box and model3d=="yes" then
				lovr.graphics.setColor(2,0,0,0.8)
				boxmodel:draw((x-3-spacex) * cellSize,
				(y-spacey) * cellSize,
				2,boxsize)
				elseif cell==box and not(model3d=="yes") then
				lovr.graphics.setColor(colors[cell])
            	lovr.graphics.cube(
				tilepng,
				(x-3-spacex) * cellSize,
				(y-spacey) * cellSize,
				2,
				tileSize)
				elseif cell==wall and model3d=="yes" then
				lovr.graphics.setColor(0.6,0.5,0.5,1)
				wallmodel:draw((x-3-spacex) * cellSize,
				(y-spacey) * cellSize,
				2,wallsize)
				elseif cell==wall and not(model3d=="yes") then
				lovr.graphics.setColor(colors[cell])
				lovr.graphics.cube(
				tilepng,
				(x-3-spacex) * cellSize,
				(y-spacey) * cellSize,
				2,
				tileSize)				
				elseif cell==storage and model3d=="yes" then
				lovr.graphics.setColor(0,0,2,0.8)
				storagemodel:draw((x-3-spacex) * cellSize,
				(y-spacey) * cellSize,
				2,0.04)
				elseif cell==storage and not(model3d=="yes") then
				lovr.graphics.setColor(colors[cell])
				lovr.graphics.box(
				tilepng,
				(x-3-spacex) * cellSize,
				(y-spacey) * cellSize,
				2,
				tileSize,tilesize,0.1)
				elseif cell==playerOnStorage and model3d=="yes" then
				lovr.graphics.setColor(0.5,1,0.5)
				playermodel:draw((x-3-spacex) * cellSize,
				(y-spacey) * cellSize,
				2,0.25)
				elseif cell==playerOnStorage and not(model3d=="yes") then
					lovr.graphics.setColor(colors[cell])
				lovr.graphics.cube(
				tilepng,
				(x-3-spacex) * cellSize,
				(y-spacey) * cellSize,
				2,
				tileSize)
				elseif cell==boxOnStorage and model3d=="yes" then
				lovr.graphics.setColor(1,1,0,0.8)
				boxmodel:draw((x-3-spacex) * cellSize,
				(y-spacey) * cellSize,
				2,boxsize)					
				elseif cell==boxOnStorage and not(model3d=="yes") then
				lovr.graphics.setColor(colors[cell])
				lovr.graphics.cube(
				tilepng,
				(x-3-spacex) * cellSize,
				(y-spacey) * cellSize,
				2,
				tileSize)
				end
              

            end
        lastx=x 
        end
    end


--lovr.graphics.cube(vrgametriggermat,550,270,0,0.15)

--lovr.graphics.cube(repeatbuttonmat,57,190,0,0.6)
--lovr.graphics.cube(lvlarrowsmat,10,250,0,0.6)


--if touchinterfaceison then
--love.graphics.draw(vrgameinterfacemat,330,100,0,0.8)
--end
if helpison then
  love.graphics.print(help,100,400)
  end
end

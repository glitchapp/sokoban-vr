changelevel= Object:extend(loadLevel)

    function maplogic:new(x,y)
        changelevel:loadLevel()
    end
    
    
    
        function maplogic:update()
		--[[if not audiosource:isPlaying() then
			if currentLevel>34 and currentLevel<40 then
				lovr.audio.play( audiosource )        
			else
				lovr.audio.stop()
			end
		end]]--
		
    end
    


require 'levels'
    
     function changelevel:loadLevel()
       if cell==player and x<5 then spacex=x-16 spacey=y-4 end --camera
       --print(currentLevel)
        level = {}
        for y, row in ipairs(levels[currentLevel]) do
            level[y] = {}
            for x, cell in ipairs(row) do
                level[y][x] = cell
            end
       
       if currentLevel==1 then 
				if level1done==false then
					dreamsunpeeling= lovr.audio.newSource( "music/DreamsUnpeeling.ogg")
					dreamsunpeeling:play()
					level1done=true
				end
        lovr.graphics.setColor(1,1,1,1)
       end
       if currentLevel==5 then 
               
        
       end
        if currentLevel==10 then
        if level10done==false then
						if dreamsunpeeling:isPlaying() then dreamsunpeeling:stop() end
							liminallounge= lovr.audio.newSource( "music/LiminalLounge.ogg")
							liminallounge:play()
					level10done=true
				end 
        end
          if currentLevel==15 then  end
          if currentLevel==20 then
          	if level20done==false then
					if liminallounge:isPlaying() then liminallounge:stop() end
						Unraveling= lovr.audio.newSource( "music/Unraveling.ogg")
						Unraveling:play()
					level20done=true
				end
		  end            
          if currentLevel==25 then  end
          if currentLevel==30 then
           if level30done==false then
					if Unraveling:isPlaying() then Unraveling:stop() end
						Excelsior= lovr.audio.newSource( "music/Excelsior!.ogg")
						Excelsior:play()
					level30done=true
			end
          end
          if currentLevel==35 then
          if level35done==false then
					if Excelsior:isPlaying() then Excelsior:stop() end
						fallingapart= lovr.audio.newSource( "music/fallingapart.ogg")
						fallingapart:play()
					level35done=true
				end
            end
          if currentLevel==40 then
           if level40done==false then
					if fallingapart:isPlaying() then fallingapart:stop() end
						corruptedspring = lovr.audio.newSource( "music/CorruptedSpring.ogg")
						corruptedspring:play()
					level40done=true
				end
            end
          if currentLevel==45 then
          	if level45done==false then
					if corruptedspring:isPlaying() then corruptedspring:stop() end
						fallingapart= lovr.audio.newSource( "music/fallingapart.ogg")
						fallingapart:play()
					level45done=true
				
				end
		   end
          if currentLevel==50 then
          if level50done==false then
						if fallingapart:isPlaying() then fallingapart:stop() end
						 theabyss= lovr.audio.newSource( "music/The Abyss.ogg")
						theabyss:play()
				level50done=true
				end
            end
          if currentLevel==55 then  end
          if currentLevel==60 then  end
          if currentLevel==65 then  end
          if currentLevel==70 then  end
          if currentLevel==75 then  end
          if currentLevel==80 then  end
          if currentLevel==85 then  end
          if currentLevel==90 then  end
          if currentLevel==95 then  end
          if currentLevel==100 then end
       
        --Level 1-5 Dark
        if (currentLevel<5) then
        boxmodel=boxmodel1
		wallmodel=wallmodel1
    
	playerpng=lovr.graphics.newTexture("/assets/player.png")
            boxpng=lovr.graphics.newTexture("/assets/l4box.png")
            wallpng=lovr.graphics.newTexture("/assets/l4wall.png")
			strpng=lovr.graphics.newTexture("/assets/l4storage.png")
			playeronstrpng=lovr.graphics.newTexture("/assets/l4playeronstorage.png")
			boxonstrpng=lovr.graphics.newTexture("/assets/l4box.png")
	
		wallpngmat=lovr.graphics.newMaterial(wallpng,1,0,1,0.3)
	boxpngmat=lovr.graphics.newMaterial(boxpng,0,1,0,0.8)
	playerpngmat=lovr.graphics.newMaterial(playerpng,1,1,1,1)
	playeronstrpngmat=lovr.graphics.newMaterial(playeronstrpng,1,1,1,1)
	strpngmat=lovr.graphics.newMaterial(strpng,0,0,1,0.5)
	boxonstrpngmat=lovr.graphics.newMaterial(boxonstrpng,1,1,0,0.8)
      
		--Level 5-10 Godsray
        elseif (currentLevel<10) then
        
            lovr.graphics.setBackgroundColor(0, 0, 0.5) 
          strpng=lovr.graphics.newTexture("/assets/l2storage.png")
          	wallpng=lovr.graphics.newTexture("assets/l2wallblau2.png")
	boxpng=lovr.graphics.newTexture("/assets/l2box.png")
	playerpng=lovr.graphics.newTexture("/assets/player.png")
	playeronstrpng=lovr.graphics.newTexture("/assets/l2playeronstorage.png")
	strpng=lovr.graphics.newTexture("/assets/l2storage.png")
	boxonstrpng=lovr.graphics.newTexture("/assets/l2box.png")
	
		wallpngmat=lovr.graphics.newMaterial(wallpng,0.2,0.2,0.5,0.3)
	boxpngmat=lovr.graphics.newMaterial(boxpng,1,1,0,0.3)
	playerpngmat=lovr.graphics.newMaterial(playerpng,1,1,1,0.3)
	playeronstrpngmat=lovr.graphics.newMaterial(playeronstrpng,0,1,1,0.3)
	strpngmat=lovr.graphics.newMaterial(strpng,0,1,1,0.3)
	boxonstrpngmat=lovr.graphics.newMaterial(boxpng,0,1,1,0.3)

          
        --Level 10-15 Scanlines Gray
        elseif currentLevel<15 then
			
            lovr.graphics.setBackgroundColor(0.5, 0, 1)
            --vrgameinterface= lovr.graphics.newTexture("/assets/touchcontroltrans.png")
            boxpng=lovr.graphics.newTexture("/assets/l4box.png")
            wallpng=lovr.graphics.newTexture("/assets/l4wall.png")
			strpng=lovr.graphics.newTexture("/assets/l4storage.png")
			playeronstrpng=lovr.graphics.newTexture("/assets/l4playeronstorage.png")
			boxonstrpng=lovr.graphics.newTexture("/assets/l4boxonstorage.png")
			
				wallpngmat=lovr.graphics.newMaterial(wallpng,1,1,1,1)
	boxpngmat=lovr.graphics.newMaterial(boxpng,1,1,1,0.1)
	playerpngmat=lovr.graphics.newMaterial(playerpng,1,1,1,1)
	playeronstrpngmat=lovr.graphics.newMaterial(playeronstrpng,0,1,1,0.5)
	strpngmat=lovr.graphics.newMaterial(strpng,0,1,1,0.1)
	boxonstrpngmat=lovr.graphics.newMaterial(boxpng,0,1,1,0.5)
        
        --level 15-20 Scanlines green
        elseif currentLevel<20 then
			lovr.graphics.setBackgroundColor(0.3, 0.6, 0.1)
            
				wallpng=lovr.graphics.newTexture("/assets/l1wall.png")
				boxpng=lovr.graphics.newTexture("/assets/l1box.png")
				strpng=lovr.graphics.newTexture("/assets/l1storage.png")
				playeronstrpng=lovr.graphics.newTexture("/assets/l1playeronstorage.png")
				boxonstrpng=lovr.graphics.newTexture("/assets/l1box.png")

        
        --level 20-25 gray white CRT Monitor
        elseif currentLevel<25 then
            lovr.graphics.setBackgroundColor(0.3, 0.2, 0.1)
            
			
        --Level 25-30 Blau purple Monitor
        elseif currentLevel<30 then
			lovr.graphics.setBackgroundColor(0.60, 0.67, 0.99)
			
        --Level 30-35 Snow
        elseif currentLevel<35 then
        
            lovr.graphics.setBackgroundColor(0.5, 0, 0)
				--Change of tiles
				wallpng=lovr.graphics.newTexture("/assets/l2wallblau.png")
				boxpng=lovr.graphics.newTexture("/assets/l2box.png")
				strpng=lovr.graphics.newTexture("/assets/l2storage.png")
				playeronstrpng=lovr.graphics.newTexture("/assets/l2playeronstorage.png")
				boxonstrpng=lovr.graphics.newTexture("/assets/l2box.png")
        
        --level 35-40 raining
        elseif currentLevel<40 then
            lovr.graphics.setBackgroundColor(0.039,0.47, 0.6)
            
            --change of tiles
				wallpng=lovr.graphics.newTexture("/assets/wall.png")
				boxpng=lovr.graphics.newTexture("/assets/box.png")
				playerpng=lovr.graphics.newTexture("/assets/player.png")
				playeronstrpng=lovr.graphics.newTexture("/assets/playeronstorage.png")
				strpng=lovr.graphics.newTexture("/assets/storage.png")
				boxonstrpng=lovr.graphics.newTexture("/assets/box.png")
            
            
        --level 40-45 sunny
        elseif currentLevel<45 then
            lovr.graphics.setBackgroundColor(0.1, 0.1, 0)
            
        --level 45-50 yellow interference
        elseif currentLevel<50 then
            lovr.graphics.setBackgroundColor(0.35, 0.35, 0)
            
            --level 50-55 strange void
        elseif currentLevel<55 then
            lovr.graphics.setBackgroundColor(0.15, 0.18, 0.30)
                  
            
            --level 55-60 Matrix godsray
        elseif currentLevel<55 then
            --change of colors
            lovr.graphics.setBackgroundColor(0.30, 0.18, 0.15)
            
            --change of tiles
				wallpng=lovr.graphics.newTexture("/assets/l2wallblau.png")
				boxpng=lovr.graphics.newTexture("/assets/l2box.png")
				strpng=lovr.graphics.newTexture("/assets/l2storage.png")
				playeronstrpng=lovr.graphics.newTexture("/assets/l2playeronstorage.png")
				boxonstrpng=lovr.graphics.newTexture("/assets/l2box.png")
                        
          --level 55->60  brown cold matrix    
        elseif currentLevel<60 then
            lovr.graphics.setBackgroundColor(0, 0, 0)
            
            --change of tiles
				wallpng=lovr.graphics.newTexture("/assets/wall.png")
				boxpng=lovr.graphics.newTexture("/assets/box.png")
				playerpng=lovr.graphics.newTexture("/assets/player.png")
				playeronstrpng=lovr.graphics.newTexture("/assets/playeronstorage.png")
				strpng=lovr.graphics.newTexture("/assets/storage.png")
				boxonstrpng=lovr.graphics.newTexture("/assets/box.png")
				--vrgameinterface= lovr.graphics.newTexture("/assets/touchcontrolwhite.png")
            
            --level 60->65  green matrix
			elseif currentLevel<65 then
				--change of colors
				lovr.graphics.setBackgroundColor(0.30, 0.18, 0.15)
				--change of tiles
				wallpng=lovr.graphics.newTexture("/assets/l2wallblau.png")
				boxpng=lovr.graphics.newTexture("/assets/l2box.png")
				strpng=lovr.graphics.newTexture("/assets/l2storage.png")
				playeronstrpng=lovr.graphics.newTexture("/assets/l2playeronstorage.png")
				boxonstrpng=lovr.graphics.newTexture("/assets/l2box.png")
            
            --level 65->70  green matrix    
            elseif currentLevel<70 then
            	
            --vrgameinterface= lovr.graphics.newTexture("/assets/touchcontrolwhite.png")
            --change of tiles
				wallpng=lovr.graphics.newTexture("/assets/l3wall.png")
				boxpng=lovr.graphics.newTexture("/assets/l3box.png")
				strpng=lovr.graphics.newTexture("/assets/l3storage.png")
				playeronstrpng=lovr.graphics.newTexture("/assets/l3playeronstorage.png")
				boxonstrpng=lovr.graphics.newTexture("/assets/l3box.png")
            
            --level 70->75  red Scanlines
            elseif currentLevel<75 then
            lovr.graphics.setBackgroundColor(0.30, 0.18, 0.15)
            --vrgameinterface= lovr.graphics.newTexture("/assets/touchcontroltrans.png")
					
            
            lovr.graphics.setBackgroundColor(0.1, 0.1, 0.1)
            boxpng=lovr.graphics.newTexture("/assets/l4box.png")
            wallpng=lovr.graphics.newTexture("/assets/l4wall.png")
			strpng=lovr.graphics.newTexture("/assets/l4storage.png")
			playeronstrpng=lovr.graphics.newTexture("/assets/l4playeronstorage.png")
			boxonstrpng=lovr.graphics.newTexture("/assets/l4box.png")
            
            --level 75->80  green scanline jungle   
            elseif currentLevel<80 then
            --change of colors
            lovr.graphics.setBackgroundColor(0.15, 0.30, 0.18)
            	--change of tiles
            	wallpng=lovr.graphics.newTexture("/assets/l1wall.png")
				boxpng=lovr.graphics.newTexture("/assets/l1box.png")
				strpng=lovr.graphics.newTexture("/assets/l1storage.png")
				playeronstrpng=lovr.graphics.newTexture("/assets/l1playeronstorage.png")
				boxonstrpng=lovr.graphics.newTexture("/assets/l1box.png")
            
            --level 80->85  brown scanline jungle
            elseif currentLevel<85 then
            lovr.graphics.setBackgroundColor(0.3, 0.3, 0.3)
                
                --vrgameinterface= lovr.graphics.newTexture("/assets/touchcontrolwhite.png")
                --change of tiles
                wallpng=lovr.graphics.newTexture("/assets/l1wall.png")
				boxpng=lovr.graphics.newTexture("/assets/box.png")
				strpng=lovr.graphics.newTexture("/assets/storage.png")
				playeronstrpng=lovr.graphics.newTexture("/assets/playeronstorage.png")
				boxonstrpng=lovr.graphics.newTexture("/assets/box.png")
     
            --level 85->90  purple interference
            elseif currentLevel<90 then
                        lovr.graphics.setBackgroundColor(0.15, 0.10, 0.30)
            
            --level 90->95  Purple godsray
            elseif currentLevel<95 then
            --change of colors
            lovr.graphics.setBackgroundColor(0.17, 0.2,0.5)
            --vrgameinterface= lovr.graphics.newTexture("/assets/touchcontroltrans.png")
          --change of tiles
				wallpng=lovr.graphics.newTexture("/assets/wall.png")
				boxpng=lovr.graphics.newTexture("/assets/box.png")
				playerpng=lovr.graphics.newTexture("/assets/player.png")
				playeronstrpng=lovr.graphics.newTexture("/assets/playeronstorage.png")
				strpng=lovr.graphics.newTexture("/assets/storage.png")
				boxonstrpng=lovr.graphics.newTexture("/assets/box.png")
  
            --level 95->100  green glitched jungle
            elseif currentLevel<101 then
            lovr.graphics.setBackgroundColor(0.3, 0, 0.3)
            
            
          --vrgameinterface= lovr.graphics.newTexture("/assets/touchcontrolwhite.png")
          			wallpng=lovr.graphics.newTexture("/assets/l2wall.png")
			boxpng=lovr.graphics.newTexture("/assets/l2box.png")
			strpng=lovr.graphics.newTexture("/assets/l2storage.png")
			playeronstrpng=lovr.graphics.newTexture("/assets/l2playeronstorage.png")
			boxonstrpng=lovr.graphics.newTexture("/assets/l2box.png")
            --change of tiles
            elseif currentLevel==101 then
            lovr.graphics.setBackgroundColor(0.3, 0, 0.3)
            
          	wallpng=lovr.graphics.newTexture("/assets/wall.png")
				boxpng=lovr.graphics.newTexture("/assets/box.png")
				playerpng=lovr.graphics.newTexture("/assets/player.png")
				playeronstrpng=lovr.graphics.newTexture("/assets/playeronstorage.png")
				strpng=lovr.graphics.newTexture("/assets/storage.png")
				boxonstrpng=lovr.graphics.newTexture("/assets/box.png")

          elseif currentLevel==102 then
            lovr.graphics.setBackgroundColor(0.1, 0.1, 0)
            
        end
    end
    
end    

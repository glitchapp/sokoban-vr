function lovr.load()
    

    player = '@'
    playerOnStorage = '+'
    box = '$'
    boxOnStorage = '*'
    storage = '.'
    wall = '#'
    empty = ' '
    currentLevel=1
    
    boxX=0
    boxY=0
    cellSize = 32
    tileSize=18
    spacex=0
    spacey=0
    
    model3d='yes'
    boxsize=2
    wallsize=0.05
    
    redgodslightdialog1done=false
	greenleveldone=false
	brownlevel60=false

	level1done=false
	level10done=false
	level20done=false
	level30done=false
	level35done=false
	level40done=false
	level45done=false
	level50done=false
    
    Object = require "classic"
     require"maplogic"
  
  require"levels"
    require"changelevel"
	changelevel=changelevel()
  
   map1 = maplogic()
   
   playermodel = lovr.graphics.newModel('/3dassets/crane/scene.gltf')
   storagemodel = lovr.graphics.newModel('/3dassets/plastic_pallet/scene2.glb')   
   --skybox= lovr.graphics.newTexture("/2dexternalassets/sky_hdr.jpg")
   
   -- set 2
   boxmodel1 = lovr.graphics.newModel('/3dassets/box_stylized/scene.gltf')
   wallmodel1 = lovr.graphics.newModel('/3dassets/brick_box/scene2.glb')   
   
   --enviromodel= lovr.graphics.newModel('/3dassets/pbr_sci-fi_modular_flooring/scene.gltf')
   --envibuildmodel=lovr.graphics.newModel('/3dassets/sci-fi_building_concept/scene2.glb')
    
    currentLevel = 1

--set 3


    function loadLevel()
        level = {}
        for y, row in ipairs(levels[currentLevel]) do
            level[y] = {}
            for x, cell in ipairs(row) do
                level[y][x] = cell
                changelevel:loadLevel(dt)
            end
        end
    end

    loadLevel()
end

function lovr.update(dt)
  map1:update(dt)
  

end

function lovr.draw()
  map1:draw()
end

joystickpressed = Object:extend()

local joystick_cooldown = 0

function lovr.axis(xaxis,yasis)
--lovr.timer.sleep( 0.2 )

		if joystick_cooldown <= 0 then local xaxis,yaxis = lovr.headset.getAxis("hand/left", "thumbstick")
        else joystick_cooldown = joystick_cooldown - 0.01
        end
        --local xaxis,yaxis = lovr.headset.getAxis("hand/left", "thumbstick")

		local playerX   -- Player position and
        local playerY   -- Adjacent position in the direction of the arrow key pressed

        for testY, row in ipairs(level) do
            for testX, cell in ipairs(row) do
                if cell == player or cell == playerOnStorage then
                    playerX = testX
                    playerY = testY
                end
            end
        end
			--find cell type below box

        -- Temporary
       -- print("player x:" ..(playerX),"y:".. (playerY))
        local dx = 0
        local dy = 0
        if xaxis >0.3 then
            dx = -1
        joystick_cooldown = 0.1
        elseif xaxis < -0.3 then
            dx = 1
        joystick_cooldown = 0.1
        elseif  yaxis > 0.3 then
            dy = -1
        joystick_cooldown = 0.1
        elseif yaxis < -0.3 then
            dy = 1
        joystick_cooldown = 0.1
		end

        local current = level[playerY][playerX]
        local adjacent = level[playerY + dy][playerX + dx]
        local beyond
        --Pushing box on to empty location
        if level[playerY + dy + dy] then
            beyond = level[playerY + dy + dy][playerX + dx + dx]
        end

        local nextAdjacent = {
            [empty] = player,
            [storage] = playerOnStorage,
        }

        local nextCurrent = {
            [player] = empty,
            [playerOnStorage] = storage,
        }

        local nextBeyond = {
            [empty] = box,
            [storage] = boxOnStorage,
        }

        local nextAdjacentPush = {
            [box] = player,
            [boxOnStorage] = playerOnStorage,
        }

        if nextAdjacent[adjacent] then
            level[playerY][playerX] = nextCurrent[current]
            level[playerY + dy][playerX + dx] = nextAdjacent[adjacent]

        elseif nextBeyond[beyond] and nextAdjacentPush[adjacent] then
            level[playerY][playerX] = nextCurrent[current]
            level[playerY + dy][playerX + dx] = nextAdjacentPush[adjacent]
            level[playerY + dy + dy][playerX + dx + dx] = nextBeyond[beyond]
        end

        local complete = true

        for y, row in ipairs(level) do
            for x, cell in ipairs(row) do
                if cell == box then
                    complete = false
                end
            end
        end

        if complete then
            currentLevel = currentLevel + 1
            if currentLevel > #levels then
                currentLevel = 1
            end
            loadLevel()
        end
end
		
